# Esipuhe: Epäonnistuminen

*Olkapäät lytyssä ja niska kyyryssä laahustan kohti väritöntä tulevaisuutta. Kasvoni kerjäävät sääliä, mutta ihmiset ympärilläni ovat liian kiireisiä huomatakseen sitä. Olen merkityksetön.* Minusta tuntuu tältä, koska olen epäonnistunut. Lähestyin minua sykähdyttänyttä ihmistä saamatta vastakaikua. Minua ei tyrmätty; sen sijaan sain kohteliaan, mutta armottoman kiinnostumattoman vastauksen.

Irtoan kehostani. Leijailen ilmassa ja näen pojan lähestymässä tyttöä. Poika vaikuttaa hiukan ihastuneelta ja tyttö on vähän kiusaantunut. He edustavat *Homo Sapiens* -lajia. Se on erittäin sosiaalinen, keräillen ja metsästäen toimeentuleva ihmisapina joka asuttaa Linnunradan Aurinkoa kiertävää Maa-planeettaa. Katselen tuolla planeetalla tapahtuvaa sosiaalista vuorovaikutustapahtumaa, monimutkaista emootioiden tanssia, luonnonilmiötä. Elämää.

Kehosta irtautuminen on kielikuva. Todellisuudessa rauhoitun ja analysoin tapahtuman tuottamaa dataa. Mikä meni pieleen?

*   *Onko hän psykopaattisella karismallaan ensin vietellyt minut ja sen jälkeen paiskannut roskakoriin?* Epätodennäköistä. Näen selvästi lämmön jonka tyttö tuo ympärillään olevaan porukkaan ja työn jota hän sen eteen tekee. Epämääräisyyden pilvi, joka psykopaatin ympärillä leijuisi, tuoksuu poissaoloaan.
*   *Vaikutinko itse psykopaattimaiselta pelurilta?* Epätodennäköistä tämäkin. Kirjoitin raa’asti omista tuntemuksistani enkä imarrellut tai käyttänyt stereoptyyppisiä kliseitä. Lisäksi lähestymisestäni paistoi epävarmuus joka tuskin on viehättävää, mutta mikä ei myöskään ole psykopaattimaista.
*   *Eikö hänellä ole aikaa minulle?* Yleensä tämä ei ole realistinen vaihtoehto. Tällä kertaa kuitenkin lähestyin poikkeuksellisen sosiaalista ja pidettyä henkilöä jolla on paljon kavereita ja uskoakseni myös ehdokkata uusiksi kavereiksi. Voi olla että olen hukkunut massaan.
*   *Vaikutinko liian kiinnostuneelta?* Kerroin jokseenkin suoraan millaisia tunteita hän minussa herätti. Ne ovat vahvoja. Voi olla että säikäytin hänet kirjoittamallani viestillä.

Ja niin edelleen. Jos yritän lähestyä tätä ihmistä uudestaan liian pian, se voi vaikuttaa epätoivoiselta ja olla ahdistavaa. Jos en lähesty ollenkaan, jää mahdollinen ystävyytemme hänen varaan. Toisaalta, meillä on yhteisiä kavereita, joten saatamme ystävystyä jopa aivan spontaanisti. Aika näyttää.

Nyt minusta alkaa tuntua voittajalta. Olen voittaja, koska sain uutta dataa ihmisyydestä ja opin uusia asioita. Olen voittaja, koska sain ainutlaatuista materiaalia tätä esipuhetta varten. Tietysti olisin voittaja myös jos olisin saanut vastakaikua. *Olen voittaja joka tapauksessa.*

> I think and think for months and years. Ninety-nine times, the conclusion is false. The hundredth time I am right.
>
> —Albert Einstein

Ainut tapa päästä eteenpäin on yrittää, yrittää ja yrittää. On kokeiltava asioita joiden toimivuudesta ei ole varmuutta—muuten paikalleen jääminen on väistämätöntä. Siispä yritykset usein epäonnistuvat. Kehitys ilman epäonnistumista on tilastollinen mahdottomuus.

*Homo Sapiens* itse on miljardien vuosien ja vielä usempien yritysten tulos. Aikaisemmin jokainen epätäydellinen solujakautuminen ja nykyisin hedelmöittyminen on uusi yritys, jonka toimivuus punnitaan elävässä elämässä. Suurin osa yrityksistä on epäonnistumisia, mutta sinä ja minä seisomme katkeamattomien onnistumisketjujen päissä. Olemme selviytyjiä.

Albert Einstein on tunnettu suhteellisuusteoriasta. Se on valtava onnistuminen. Suhteelisuusteoriaa Einsteinin mielessä edelti—ja seurasi—lukuisa määrä epäonnistuneita yrityksiä, ideoita jotka jälkikäteen vaikuttavat suorastaan idiootimaisilta. Einstein oli menestynyt fyysikko, mutta ennenkaikkea hän oli epäonnistuja.

Myös ihmisen immuunipuolustusjärjestelmä kehittyy tällä tavalla. Vastasyntyneen lapsen immuniteetti on heikko. Sitten lapsi altistuu bakteereille ja viruksille. Puolustusjärjestelmä epäonnistuu ja lapsi sairastuu. Sitten järjestelmä oppii virheestään ja immuniteetti kehittyy.

Epäonnistuminen sattuu. Kun lapsi opetellessaan kävelemään kaatuu ja lyö päänsä, itku tulee ja räkä valuu. Prosessia on kuitenkin mahdollista parantaa oppimistehon kärsimättä siten, että kipua tulee vähemmän. Nykyään polkupyöräilyä opettelevan lapsen päässä nähdään usein kypärä. Rokotus opettaa immuuniksi taudille ilman sen kärsimistä koko kauheudessaan.

<!--Minä tiedän evoluution voiman ja uskallan todeta olevani epäonnistuja.-->

Olen koko lyhyen elämäni ajan etsinyt kiehtovia asioita. Olen kokeillut musiikkia, fysiikkaa, matematiikka ja huumeita. Kaikki nämä ovat opettaneet minulle paljon, mutta lopulta ihmiset ovat syrjäyttäneet ne elämässäni. Minulle ihmiset ovat ykkösjuttu.

*   Järjestän asiani siten, että minulla on paljon aikaa ja voimia käyttää muihin ihmisiin.
*   Pidän vanhoja ihmissuhteita yllä puhelimella, kirjeillä ja matkustamalla. En odota toisten tekevän samaa.
*   Etsin uusia ihmisiä ja yritän tutustua ihmisiin jotka koen mielenkiintoisiksi.
*   Tunnemyrskyn iskiessä selvitän mistä on kyse ja vältän tuhoisia ratkaisuja.

Lopputuloksena minulla on paljon ystäviä. Ystävillä en tarkoita facekavereita tai muita hyvänpäiväntuttuja—kyse on syvemmästä asiasta. Pohjimmiltaan ystävyys on ongelmanratkaisua yhdessä.

Kyllä. Elämä on työtä, tarkemmin sanottuna työtä termodynamiikan toista pääsääntöä vastaan. Jos et tiedä mikä tuo toinen pääsääntö on, ei se haittaa kovin paljoa—tärkeää on se, että työskentelemme ratkaistaksemme ongelmia, ja että ystävyys on tuon työn tekemistä yhdessä.

Monen korvaan sana “työ” kuulostaa ikävältä. Yhteiskunta opettaa että työstä ei voi nauttia. Tämä on yksinkertaisesti valetta. Tehokkain työ on sellaista mikä tuottaa nautintoa, mitä tulee tehtyä ilman että itseään täytyy piiskata.

Fysiikassa työ tarkoittaa sitä että jotakin kappaletta liikutetaan vastusvoimista piittaamatta. Sähkökenttä tekee työtä elektronin hitausvoimaa vastaan kiihdyttäen sitä. Sähkökenttää ei tarvitse komentaa—se tekee tämän puhtaasti omasta tahdostaan.

Työ jota teen ystävieni kanssa on samankaltaista kuin sähkökentän tekemä työ. Ratkaisemme ongelmia kaljan hankkimisesta hyväksikäyttäjän hätistämiseen. Kyse on vimmaisesta voimasta joka jokaisessa solussamme nukleiinihapon muodossa virtaa. Me olemme fysiikkaa, ja yhdessä olemme käsittämättömän voimakas luonnonilmiö.

Se mitä yhteiskunta työksi kutsuu on vain kalpea kopio tuosta ilmiöstä. “Työmoraali” on ideologia joka on saanut aikaan paljon pahaa. Todellinen työ ei moraalia tuekseen tarvitse.

<!--En ole varma tulisiko minun käyttää sanaa “kaveri” vai “ystävä-->

<!-- Voisin käyttää sanan “kaveri” sijasta sanaa “ystävä”. Ensimmäinen yhdistetään usein kevyempään, ehkä lyhyempiaikaiseen hengailuseuraan kuin jälkimmäinen, mutta koska jälkimmäiseen yhdistetään usein jotain yliluonnollista, esimerkiksi telepatiaa tai muu maaginen yhteys, olen päätynyt käyttämään vain sanaa “kaver”. -->

Nyt kun ymmärrämme ystävyyden yhteisenä onglemanratkaisutyönä, voimme vastata klassiseen kysymykseen. Voivatko tyttö ja poika olla ystäviä? Kyllä voivat! Lapsen kasvattaminen on tyypillinen asia mitä tyttö ja poika tekevät yhdessä. Se jos mikä on yhteistä ongelmanratkaisua.

Yksi arvokas asia ystävyydessä on toisen poikkeava perspektiivi. Samalla tavalla ajattelevat ystävät eivät tuo uusia tuulia; pahimmillaan he vain vahvistavat omia erheellisiä käsityksiä. Vastakkaista sukupuolta olevat ihmiset ajattelevat merkittävästi eri tavoin, joten jossain mielessä he ovat jopa tärkeämpiä ystäviä kuin oman sukupuolen edustajat.

Seksuaalisuus tuo tietenkin hommaan joitakin haasteita. Jos panettaa kovasti tai on hurjan rakastunut, voi tulla tehtyä järjettömiä ratkaisuja. Mutta ei hätää, typeryyksiä voi oppia välttämään. Se on mahdollista tappamatta seksuaalisia tunteita—niiden voima on itseasiassa valjastettavissa kokonaan rakentavaan työhön.

<!--Työmoraali ja monet muut yhteiskunnassa leviävät valheet näyttävät johtaneen siihen että monella tuntuu olevan vaikeuksia saada ystäviä. Tämä on ikävää, mutta minulla on hyviä uutisia: asiaan on mahdollista vaikuttaa -->

Monella tuntuu olevan vaikeuksia löytää ystäviä. Periaatteessa kyse on yksinkertasesta asiasta: on tutustuttava uusiin ihmisiin. Mitä useampaan ihmiseen tutustuu, sen paremmat mahdollisuudet hyvien yhteistyökumppanien löytymiseen ovat. Yhteistyö ei suju ilman rehellisyyttä, joten rohkeutta olla oma itsensä tarvitaan.

*Ole vain rohkeasti oma itsesi!* Kuten monet kliseet, tämä on täysin hyödytön ohje—kaikki tietävät että rehellisyys maan perii. Tarvitsemme käytännöllisiä ohjenuoria siihen kuinka päästä kohti tuota jaloa ideaalia. Ystäviä halajava tarvitsee muutamia yksinkertaisia asioita:

*   Aikaa ja energiaa.
*   Tilaisuuksia kohdata uusia ihmisiä.
*   Ymmärrystä siitä millainen ihminen itse on.
*   Taitoa pärjätä ikävien tunteiden kuten mustasukkaisuuden ja itsesäälin kanssa.
*   Taitoa tunnistaa tarkoituksellista ilkeilyä ja huijauksia.

Simppeleillä käytännön valinnoilla ja miettimisellä on näitä kaikkia asioita mahdollista hankkia. Rohkeus olla oma itsensä seuraa perässä.

Merkittävää on että luontaista sosiaalista lahjakkuutta ei tarvita. Minulla on diagnosoitu Aspergerin oireyhtymä ollessani ala-asteen toisella luokalla, ja nuorempana olenkin ollut kömpelö monissa sosiaalisissa tilanteissa. Sittemmin laajan sosiaalisen kokemuksen myötä olen oppinut paremmaksi.

Olen myös oppinut suhtautumaan rakentavasti niihin autistisiin kiviin joihin vielä kompastelen. Nykyään ne ovat minulle ja ystävilleni sekä huumorin että inspiraation lähde. Mokaan, ja kaverit sanovat, “Ei vittu miten sä voit olla noin burgeri äijä!” Nauramme ja ihmettelemme kuinka kujalla ihminen voi olla. (Asperger `->` assburger `->` burger.)

Autistinen henkilö nähdään usein vastakohtana sosiaalitaiturille josta stereotyyppisenä esimerkkinä käy myyjä, poliitikko tai pickup artist. Tosiasiassa tällaisen taiturin kyvyt eivät ole luonteeltaan sosiaalisia—poliitikko tai myyjä harvoin pyrkii yhteistyöhön kuulijoidensa kanssa. Politiikassa ja myynnissä tarvitaan yhteistyössä auttavien taitojen sijaan antisosiaalisia harhautustaitoja.

Hyvä esimerkki on anteeksipyytämisen taito. Lapsille opetetaan, että sen jälkeen kun on lyönyt toista, tulee pyytää anteeksi. Tämä on antisosiaalinen taito joka auttaa pääsemään pälkähästä väkivallanteon tai muun ikävän toiminnan jälkeen.

Todellinen anteeksipyytämisen taito on sitä että osaa ja uskaltaa ilmaista katumuksensa *jos sitä sattuu tuntemaan*. Taitava anteeksipyytäjä ei pyydä anteeksi lyöntiä jos hän koki sen oikeaksi ratkaisuksi. Toisaalta hän pyytää tilanteeseen sopivaa elettä käyttäen anteeksi jokaista tekemäänsä asiaa minkä hän huomaa olleen ikävää toisіlle.

Yleisesti ottaen sosiaalinen keskustelutaito on sitä että osaa päästää suustaan ne tosiasiat jotka auttavat keskustelukumppania eteenpäin. Kaikkea ei voi sanoa—aika ei yksinkertaisesti riitä—joten on valittava. Sanonko että kaverini tatuointi näyttää rumalta silmääni? En, sillä kaverini tuskin tarvitsee mielipidettäni mihinkään. Sanonko että kaverini sepalus on auki? Kyllä, koska hän ei luultavasti sitä tiedä voi helposti korjata asian.

<!--Sosiaalisesti taitava henkilö ei ole cool. Hän on näkymätön, pettämätön,-->

<!-- EndOfContent -->

Seuraavaksi: [Luku 1. Sosiaalinen talous](https://gitlab.com/konstakurki/kaveruudesta/blob/master/luku01.md)

















