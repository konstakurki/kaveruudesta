# Käytännön kaveruus

Ensimmäinen luku oli jokseenkin teoreettinen yhteenveto siitä mitä kaveruus on ja kuinka se toimii. Nyt sukellamme käytännön ilmiömaailmaan ja menetelmiin joita kaverien kanssa havaitaan ja tarvitaan.

## Kenestä on kaveriksi?

“Kaikki ovat niin tyhmiä,” kuulen sanottavan. Tai itsekkäitä, tai tylsiä. “Ei heistä ole kavereiksi.” Nämä ovat yleisiä harhakäsityksiä. Todellisuudessa suurin osa lähestulkoon mistä tahansa ihmisryhmästä on yhteistyöhaluisia ja pääosin rehellisiä ihmisiä, toisinsanoen potentiaalisia kavereita.

Voisi ajatella että joistakin ryhmästä kavereita löytyy helpommin tai että jostakin ryhmästä löytyvät kaverit ovat parempia, mutta oman kokemukseni mukaan ryhmien väliset erot ovat pieniä. Kavereita kannattaa etsiä suurinpiirtein kaikkialta, tai ainakin sieltä missä itse sattuu olemaan, ilman sen kummempaa ennakkoarvottamista.

Tämä on fakta, ei mikään tasa-arvohurskasteluun perustuva kaunis ajatus. On myös totta että kaikista ihmisistä ei ole kavereiksi, mutta tällaiset antisosiaaliset huijarit—noin 1%–10% ihmispopulaatiosta—ovat levittäytyneet kaikkiin ihmisryhmiin. Ryhmien pienet erot juontuvat pääasiassa siitä kuinka hyvin huijaamisella on mahdollista menestyä. Myöhempi luku tästä kirjasta on omistettu huijareille.

Itse olen karttanut ainakin humalahakuisesti juovia ja tupakkaa polttavia ihmisiä, homoseksuaaleja, räp-, metalli- ja elektronisen musiikin tykkääjiä, politiikasta kiinnostuneita sekä humanisteja (vastakohtana luonnontieteilijöille). Usein ihmisryhmän karttaminen saa alkunsa sellaisesta mahdollisesti aivan oikeasta havainnosta että ryhmän edustajat ovat jossakin asiassa väärässä. Sitten virheellisesti tulee pääteltyä että he eivät kykene mihinkään tolkulliseen touhuun. Yleisyydestään huolimatta ei tällaisessa päättelyssä ole mitään järkeä; joka iikka on jossain asiassa väärässä ja jossain oikeassa.

Erimielisen ihmisryhmän karttaminen saattaa johtua myös siitä, että pelkää erimielisyyden aiheuttavan konfiktin. Tämä on epäjärkevää kahdessa mielessä: ensinnäkin ei-liian-raju konflikti toimii usein arvokkaana oppimiskokemuksena. Toiseksi, konfliktia ei välttämättä tule, mikäli asiaan suhtautuu sopivan kevytmielisesti.

Kolmas syy karttaa ihmisryhmää on pelko siihen samaistumisesta. Itse kartan transsukupuolisia ihmisiä vielä tänäkin päivänä, sillä se edustaa suurta haastetta omalle jokseenkin suoraviivaiselle heteroseksuaalille identiteetilleni. Pohdin, tai pelkään pohtia, “Eikö seksuaalisuus olekaan jakautunut kahdeksi eksaktiksi pisteeksi? Missä itse olen tuolla jatkuvalla janalla?” Kysymyksen kohtaamisen sijaan lakaisen sen maton alle. Tämä pelko on maksanut minulle ainakin yhden kaverin.

Eräs paragmaattinen syy miksi kranttuilusta kannattaa pyrkiä eroon on se, että erilailla ajattelevat ihmiset auttavat harhakäsityksistä eroonpääsemisessä. Melkein jokainen ajattelee olevansa oikeassa, mutta kaikkien kohdalla se ei voi pitää paikkansa. Miksi juuri minä olisin oikeasti oikeassa? Ainut tapa karsia omia harhakäsityksiä on törmätä vaihtoehtoisiin käsityksiin ja vertailla. Tämä onnistuu parhaiten erilaisia käsityksiä omaaviin ihmisiin tutustumalla.

Itse ajattelin joskus että elektroninen musiikki on tyhjää ja sen harrastajat jollain tavalla huonompia ihmisiä kuin esimerkiksi jazzarit. Sitten tutustuin elektronisen musiikin innokkaaseen harrastajaan. Hänetä tuli yksi parhaimmista kavereistani, ja innostuin itsekin elektronisesta musiikista. Innostuin siitä niin paljon että aloin itse tekemään sitä. Aikaisempi käsitykseni elektronisesta musiikista ja sen harrastajista oli niin harhainen kuin käsitys voi vain olla.

Minkä tahansa ryhmän sisällä on valtava määrä harhakäsityksiä. Sen takia puhutaan erilaisista “kuplista”. Jos viettää koko elämänsä yhden ryhmän sisäpuolella, on mahdotonta saavuttaa kovin suurta viisautta, olipa ryhmä kuinka älykäs tahansa. Siispä vaihda kuplaasi usein, ja jos mahdollista, ole yhtäaikaa useamman kuplan sisällä.

Ryhmän mukaan luokittelu ei ole ainoa tapa muodostaa ennakkokäsityksiä. Ihmisiä voi arvioida muutenkin: näyttää alykkäältä tai näyttää tyhmältä, esimerkiksi. Myöskin tällaisen arvioinnin perusteella kranttuilu on haitallista. Se, kuinka hyvin yhteistyö ihmisen kanssa sujuu, ei juurikaan korreloi minkään helposti arvioitavan ominaisuuden kanssa. Ainut tapa selvittää asia on yrittää kaveruutta.

En tarkoita sitä, etteikö ihimisiä pitäisi arvoida pinnallisten seikkojen perusteella. Itse arvioin ulkonäön perusteella sekunnissa, “tyhmä”, “kömpelö”, “heikkoitsetuntoinen”. Jos käyttäisin näitä arvioita ihmisten karttamiseen, olisi minulta jäänyt valtava määrä hienoja ihmisiä löytämättä ja typeriä uskomuksiani huomaamatta. Arvioinnissa ei ole mitään vikaa, mutta sen perusteella karttaminen on yleensä typerää.

*   rikollisuus
*   rappio

## Kaverien etsiminen

Kavereita kannattaa etsiä kaikista maailman ihmisistä. Mikäli sosiaalitaloudellinen tilanne on todella yksinäinen ja pula kavereista on huutava, kannattaa heitä tietysti etsiä erityisesti sellaisista ihmisistä jotka itsekin kaipaavat kavereita. Ollessani yksin ja toimettomana Brysselissä eräs koditon mies tuli kysymään minulta rahaa. Hän vaikutti mukavalta ja annoin hänelle sen mitä hän pyysi. Keksin sanoa hänelle, “Olen täällä yksin eikä minulla ole mitään tekemistä. Haluaisitko hengailla kanssani jonkin aikaa?” Mies ilahtui ja vaihdoimme kokemuksiamme ja ajatuksiamme parin tunnin ajan.

Yksinäisiä ihmisiä ihmisiä on maailma pullollaan—et ole yksin. Kodittomien lisäksi vanhukset ovat usein yksinäisiä. Jos mummo tarvitsee apua, mene auttamaan. Facebookissa on ryhmiä joissa ihmiset itsetarkoituksellisesti etsivät uusia [kavereita](https://www.facebook.com/search/groups/?q=uusia%20kavereita) ja [ystäviä](https://www.facebook.com/search/groups/?q=uusia%20yst%C3%A4vi%C3%A4). Jos joku tulee tekemään tuttavuutta kanssasi, tartu tilaisuuteen. Se, että itse ei tarvitse lähestyä, on luksusta.

Jos olet yksinäinen, voit sanoa sen suoraan, kuten minä sanoin Brysselissä. Mikäli onnistut välttämään epätoivoista mielentilaa, on yksinäisyytesi sinulle valttikortti: se tarkoittaa että tarvitset toista toden teolla. Se saa toisen tuntemaan itsensä tärkeäksi, se antaa hänelle tilaisuuden olla sankari. Toisaalta, jos olet kovin epätoivoinen, yksinäisyys tuskin riittää tekemään sinusta kokonaisuudessaan viehättävää ihmistä.

Epätoivon välttäminen yksinäisyydessä on hankalaa. Itse pelkäsin pitkään lyhyitä tuttavuuksia; pelkäsin hyvästien sanomista. Sitten, kesällä 2015, kaverini ehdotti minulle liftausreissua. Olin kauhuissani. Hän sai kuitenkin ylipuhuttua minut lähtemään matkaan. Reissu oli mahtava; innostuin niin paljon että seuraavan vuoden ajan liikuin lähes pelkästään liftaamalla. Opin sinä aikana arvostamaan muutaman minuutin mittaisia tuttavuuksia siinä missä vuosien kaverisuhteitakin.

Liftaaminen on erinomainen tapa tutustua uusiin ihmisiin. Kun autoilija poimii liftarin kyytiin, on tilanne spontaani eikä muodollisia normeja ole. Autoilija on luultavasti juttuseuran tarpeessa, joten keskustelu syntyy lähes automaattisesti. Liftaaminen opettaa kuljettamaan keskustelua ja etsimään yhteisiä puheenaiheita kaikenlaisten ihmisten kanssa.

Kerran liftatessa kaverini kanssa Tampereelta Jyväskylään jäimme tienpäälle. Istuimme matkatavaroiden kanssa kaupan pihalla ja pohdimme mitä tehdä. Sitten joku tuli kysymään tulta tupakkaansa. Emme voineet auttaa. “Kyllä mulla oikeesti on tulta, piti vaan tulla kattoon että mitä ihmettä te teette täällä.” Selitimme tilanteemme johon kaveri vastasi, “Mulla on tuolla kesämökki. Voitte tulla sinne yöksi jos haluatte.” Juttelimme kaverin kanssa pitkään. Seuraavana päivänä hän heitti meidät tien varteen josta pääsimme nopeasti Jyväskylään. Kaverin toiminta oli esimerkillistä: hän oli yksinäinen ja huomasi tilaisuutensa tulleen. Hän ymmärsi kesämökin tarjoavan hänelle etulyöntiaseman.

Suurin osa liftituttavuuksista on ohikiitäviä maksimissaan muutaman tunnin mittaisia välähdyksiä pimeässä. Suomi on kuitenkin pieni maa, ja minut on neljä kertaa poiminut kyytiin sellainen henkilö jonka kyydissä olen ollut aikaisemmin. Mikään ei myöskään estä yhteystietojen vaihtamista kyydin päätteeksi. Itse olen pysynyt jonkinlaisessa kontaktissa ehkä joka 70. liftaustuttavuuteni kanssa.

Liftailin pääasiassa Suomessa, maassa jonka kielen, kulttuurin ja kansan tunnen läpikotaisin. Ulkomailla matkustaminen tarjoaa toisenlaisia mahdollisuuksia kavereiden löytämiseen. Toisesta kulttuurista tuleva on lähes aina mielenkiintoinen, joten paikalliset ovat aina innokkaita tutustumaan. Lisäksi matkalaisen oletetaan tarvitsevan neuvoa ja muuta apua, joten kenelle tahansa on OK mennä puhumaan.

Toisaalta kavereiden kanssa matkustaessa voi olla hankalaa päästä kunnolliseen kontaktiin paikallisten kanssa. Kun olin kavereideni kanssa Kroatiassa ja Sloveniassa, laitoin ujouden takia heidät aina puhumaan puolestani. En puhunut kenenkään paikallisen kanssa itse. Halusin tehdä muutoksen tilanteeseen, ja niinpä eräänä aamuna sanoinkin, “Jatkakaa matkaa, minä jään hetkeksi kulkemaan itsekseni.” Heti kun he lähtivät, pakotti tilanne minut vaihtamaan majapaikkamme emännän kanssa muutamia sanoja. Jää särkyi täysin ja keskustelimme neljä tuntia. Sitten lähdin talsimaan peukalo pystyssä kohti Kroatian ja Slovenian rajaa. Ensimmäinen auto otti minut kyytiin. Illalla kymmenen maissa näin taas kaverini Slovenian pääkaupungissa Ljubljanassa.

Ulkomailla voi olla myös hankalaa päästä keskustelussa syvemmille vesille. Juttu jää helposti siihen että Suomessa on kylmä, paitsi jos sauna on lämmin ja pullossa on viinaa. Ja toisaalta, mikäli jonkun kanssa onnistuu kaverustumaan oikein kunnolla, on kaveruussuhteen jatkaminen tulevaisuudessa hankalampaa. Vielä toisaalta: minulla on kaveri Kanadassa, jonka haluan tavata. Se motivoi matkustamaan sfsd

Kavereita voi etsiä myös Tinderistä. Se on deittisovellus jossa yksinkertaisen profiilin perusteella päätetään onko lähellä oleva ihminen mukiinmenevä vai ei. Jos kaksi ihmistä tykkäävät toisistaan, muodostavat he “matchin” ja pääsevät chattailemaan toisilleen.

Tinder on brändätty seurustelu- tai seksikumppanin etsimistä varten, mutta se ei estä Tinderin käyttämistä kaverien etsimiseen. Sitäpaitsi pohjimmiltaan sekä seksi- että seurustelukumppanit ovat kavereita; tarkan rajan vetäminen on järjetöntä. Tinder on rajaton uusien ihmisten lähde jossa voi epäonnistua niin monta kertaa kuin jaksaa. On tilastollisesti lähes varmaa että jossain vaiheessa sieltä löytää kaverin itselleen.

Eräs kaverini on vankimielisairaalassa. Hän on yksinäinen. Tällä hetkellä meillä on projektina hommata hänelle nykyaikainen puhelin jotta hän voisi käyttää Tinderiä.

## Joustavuus

Se että hyväksyy kaikenlaiset ihmiset kavereikseen on hyvä lähtökohta, mutta vielä parempi on jos kykenee itse ottamaan osaa monenlaisiin toimintamalleihin. Yksinkertainen esimerkki on tupakka. Jos osaa polttaa tupakan ja vieläpä nauttia siitä, on mahdollista osallistua tupakanpolttohetkeen. Silloin kun ollaan tupakalla, on tunnelma aivan omanlaisensa. Keskustelu on erilaista ja tupakanpolttajien keskuudessa on yhteisymmärrys. Tupakkahetkeen osallistuminen on myös selvä osoitus siitä että ei halveksi tupakoijia.

En kehota polttamaan tupakkaa liikaa. Kyse on kyvystä osallistua tupakanpolttohetkeen tarvittaessa. Myöskään kirjaimellinen tupakan polttaminen ei ole välttämätöntä; itse olen joskus laittanut tupakan suuhun mutta jättänyt sen sytyttämättä ja vain teeskennellyt polttavani sitä. Ja voihan tupakalle lähteä ihan vain seuraksikin.

Myös kahvinjuonnin taito helpottaa ihmisten kanssa toimimista, mutta kaikkein merkityksellisin päihde on alkoholi. Erittäin moni sosiaalinen tilanne pyörii tuopin tai viinilasin ympärillä. Niihin voi tietysti osallistua juomalla alkoholitonta juomaa, mutta sellainen toimintaa asettaa henkilön joukon ulkopuolelle ja saattaa vaikuttaa hurskastelulta tai muuten vain kummalliselta. Vaikka ei alkoholia päihtymistarkoituksissa haluaisikaan juoda, osoittaa tuopin tai kahden kaataminen ymmärrystä juomakulttuuria kohtaan.

Alkoholin päihdyttävää vaikutusta ei myöskään kannata pelätä tai aliarvioida. Itse pelkäsin alkoholia kunnes vedin ensimmäiset kännini 17-vuotiaana. Sitten aloin juomaan kuten muutkin ikäiseni ja kaverieni määrä lähti hurjaan nousuun. Humalan avulla uskalsin mennä tilanteisiin jotka selvinpäin olisivat olleet herkälle nuorelle liian pelottavia, ja opin sosiaalisia taitoja ja itsevarmuutta. Nykyään uskallan mennä melkein kaikkiin sosiaalisiin tilanteisiin selvinpäin, mutta en usko että tilanne olisi sama jos en olisi koskaan maistanut alkoholia.

Joissain porukoissa kiroileminen on yleistä. Kiroilusta pidättäytyminen voi sellaisessa ryhmässä vaikuttaa jeesustelulta. Rohkeus kirota antaumuksella ja monipuolisesti on monessa tilanteessa etu. Kiroilua ei tietysti tarvitse harrastaa joka tilanteessa. Itse kiroilen todella laveasti läheisten kavereideni seurassa. Keksin itse yhdyskirosanoja kuten “kyrvänmutka”, “perkeleenreikä” ja “ripulipillu”. Käytän myös voimasanoja joita ei normaalisti mielletä kirosanoiksi, esimerkiksi sanoja “seksuaali” ja “tyryntyltty”.

*   Rasistinen, sadistinen, seksistinen
    ja alatyylinen huumori sekä pissakakkajutut

Pelkkä mekaaninen toimintaan osallistuminen ei tietysti riitä—siitä täytyy nauttia. Itseään ei voi komentaa nauttimaan, mutta tupakointi, ryyppääminen, kiroileminen ja epäkorrekti huumori ovat kaikki kaikki luonnostaan hyvältä tuntuvia asioita. Kun niitä alkaa tehdä, alkaa niistä myös nauttia. Vaarana on tietysti koukuttuminen. Minä suojelen itseäni tupakkakoukkuun jäämiseltä polttamalla vain silloin kun joku muukin polttaa. En myöskään osta tupakkaa juuri koskaan; jos poltan, pummaan tupakan kaverilta. Pummaamisessa—kuten myös tarjoamisessa—on se lisäetu että pummaajan ja tarjoajan välinen suhde syvenee.

<!--Itsetään voi tehdä profitablen myös tarjoutumalla käytännölliseksi avuksi, mikäli avunanto muodostaa samalla rennon sosiaalisen vuorovaikutustilanteen. <- pitää lisätä lukuun 1-->

<!--Jos sinut kutsutaan juhliin, mene, vaikka epäilisit että kutsu on annettu säälistä.-->

<!--Muistan, kuinka joskus kirjoitin Muusikoiden.net -keskustelupalstalle, “Kaverin elektronisen musiikin harras-->

<!--*   opiskelijahaalarit
*   perusjutut: jos joku kutsuu juhliin,
    mene. sopeudu muiden aikatauluihin, yms.-->

<!-- Olen melko varma että alkoholin ansiosta minulla on huomattavasti enemmän kavereita ja olen itsevarmempi ja sosiaalisesti sujuvampi.-->

<!--tinderissä kannattaa avata niin monta keskustelua kuin jaksaa-->

## Tutustuminen

Kaverisuhteen alkupää on tutustumisen aikaa. Siinä henkilöt oppivat asioita toisistaan ja hakevat yhteistä toimintatapaa. Aivan alussa tutustuminen on vuoropuhelua joka koostuu kysymyksistä ja vastauksista. “Opiskeletko jotain?” “Joo, olen fysiikan laitoksella Jyväskylässä. Aivan sysipaska paikka kertakaikkiaan. Mites sä?” Kysymys-vastaus-keskustelu ei kuitenkaan kykene muuhun kuin pinnallisten tiedonjyvien vaihtamiseen. Se on sama kuinka syvällisestä aiheesta keskustelua käydään; jos keskustelussa ainoastaan vaihdetaan tietoa, ei se rakenna suhteeseen syvyyttä.

Tutustuminen alkaa kunnolla siinä vaiheessa kun henkilöt käyvät jonkun ongelman kimppuun yhdessä—toisin sanoen tutustuminen alkaa samalla hetkellä kuin itse kaveruus. Ongelmaa ratkaistaessa henkilöt saavat kokemusta toistensa mielten toiminnasta ja muodostavat itse käsityksensä. Sellainen tieto on hyvin abstraktia eikä sitä voi sanoiksi pukea millään. Sitä ei voi myöskään tekaista, toisin kuin vastauksia kysymyksiin. Siispä, jos hiuksia halotaan, ei tutustumista ja kaveruutta voi oikeastaan edes erottaa toisistaan.

Tinderissä ensimmäinen ongelma kohdataan viimestään siinä vaiheessa kun päätetään nähdä: milloin nähdään, ja missä? Se on ongelma joka vaatii todellista yhteistyötä ratketakseen. Tutustumista voi nopeuttaa nostamalla ongelmia tietoisesti pöydälle—koulutuksesta kysymisen sijaan voi esimerkiksi kysyä näkemystä omassa elämässään kohtaamaansa ongelmaan—ja tapaamalla jonkun käytännöllisen projektin merkeissä. Jos tarjoudut muuttoavuksi, saat kaupanpäälle yhtä monta ongelmaa kuin kämpässä on tavaroita.

> Conflict defines us.
>
> —Pieter Hintjens kirjassaan [*Culture & Empire*](http://cultureandempire.com/)

Perstuntuma sanoo usein että konfliktia kaverin kanssa tulee välttää. Esimerkiksi ristiriidat arvoissa ja tavoissa hoitaa käytännön ongelmia on helppo lakaista maton alle. Miksi kaivaa ikäviä tunteita esiin kun niitä voi myös välttää? Eikö se olisi vain ilkeyttä?

Kuten Salkkareita seuranneet tietävät, saattaa konfliktin välttelyllä olla pitkässä juoksussa tuhoisia seurauksia, mutta niiden kohtaamiseen on olemassa vielä painavampikin syy. Kaverusten välisten keskinäiset konfliktit ovat nimittäin kaikista hedelmällisimpiä ongelmia tutustumisen kannalta. Ne vaativat suurta empaattista työpanosta ratketakseen. Mitä suurempi konflikti, sitä syvämmäksi se suhdetta ratketessaan rakentaa. Tietysti liian suuri konflikti voi olla tuhoisaa suhteelle.

Asiaa voi verrata kuntoiluun: lihaksen tulee ensin hajota pikkuisen kasvaakseen. Ilman kipua ei tule kuntoa. Sitten, jos repii itseään liikaa, tulee liikaa kipua ja paikat menee paskaksi. Tämä kannattaa muistaa kun kaverin kanssa on kiusallinen, vihainen tai muuten vain ikävän tuntuinen hetki. Sellainen hetki sisältää aina siemenen syvämpää kaveruutta kohti.

<!--## Ylläpito

Joskus ka

## Lukeminen

## Seksuaalisemmat suhteet-->

## Autisteille

Autistiset piirteet ovat jokseenkin yleisiä. Perheessäni esiintyy niitä; itse sain lääketieteellisen Aspergerin oireyhtymä -diagnoosin (F84.5) ollessani ala-asteen toisella luokalla. Autismi saattaa aiheuttaa suuria ongelmia ihmisten kanssa toimimisessa, mutta ymmärtämällä mistä on kyse on mahdollista vaimentaa niitä.

Autismi on pohjimmiltaan vaistomaista oman ajattelun rankkaa painottamista yli muiden tietolähteiden. Kyse ei ole omien ajatuksien palvomisesta tai jumaloinnista tai toisaalta tuntemattoman pelosta, vaan puhtaasti taipumuksesta olla huomaamatta muita vaihtoehtoja. Jos maisemaa katsoo kiikarit päässä, näkee pikkujuttuja tarkasti, mutta koko maisemasta on vaikea saada käsitystä. Siinä missä jollain ei ole kiikareita käytettävissä ollenkaan, on autistilla ne sisäänrakennettuna silmiin.

Käytännössä autistinen henkilö tekee päätelmän ja luottaa siihen. “Tuo henkilö sanoi typerän asian. Siispä häntä ei kannata kuunnella.”  Epämääräisessä maailmassa aukoton päätelmä on harvinaisuus, ja siten autismi aiheuttaa ongelmia. Toki joissakin tapauksissa autistinen mielenlaatu on myös vahvuus. ”Putoavassa hississä oleva mies lilluu lattia yläpuolella eikä hän paina mitään. Siispä painovoimaa ei ole olemassa; sen sijaan maanpinta on kiihtyvässä liikkeessä kohti putoavaa hissiä.” Albert Einstein piti tästä yksinkertaisesta ajatelmasta kiinni kahdeksan vuotta, ja lopputulos oli yleinen suhteellisuusteoria.

Sisäänrakennettuja kiikareita on vaikea laskea alas. Mutta ei hätää! Kiikaritonta, laajaa näkökulmaa voi emuloida heiluttelemalla kapeakatseisia kiikarisilmiänsä ympäriinsä. Tämän asian ymmärtäminen on ollut elämäni tärkein oivallus. Tarkemmin sanottuna olen oivaltanut että oikeassa olemisen tunteeseen, olipa se kuinka vahva tahansa, ei kannata luottaa sokeasti. Tähän oivallukseen luotan autistisella varmuudella, ja kokemus sanoo että tälläkertaa se kannattaa. Käytännössä pyrin tietoisesti huomaamaan ja romuttamaan elämääni hallitsevia yksinkertaisia kaavoja.

Kokemuksesta puheen ollen on olemassa yksi tapa tietää että joku ajatus on järkevä. Se on kokeilla ajatusta uudelleen ja uudelleen. Jos ajatus toimii kerta toisensa jälkeen, on siinä tolkkua. Muutoin se joutaa romukoppaan. Toimintatavan rutiininomainen toistaminen osoittaa että niin voi toimia, mutta se ei kerro mitään siitä etteikö parempia (ehkä jopa 1000 kertaa parempia) vaihtoehtoja olisi olemassa.

Autistille voin antaa muutamia aivan konkreettisia ohjeita.

*   Tapaa ihmisiä jotka vaikuttavat typeriltä.
*   Tee asioita jotka vaikuttavat typeriltä.
*   Erityiset kiinnostuksenkohteesi ovat muiden mielestä typeriä, ja monesti he ovat siinä oikeassa. Älä hae sympatiaa niille; sen sijaan naura niille muiden kanssa. Parhaimmassa tapauksessa niistä saa revittyä loistavaa huumoria.
*   Älä esitä kiinnostunutta jos et sitä ole; siitä jää aina kiinni ja keskustelukumppanille tulee kiusallinen olo. Sen sijaan keskustele avoimen epäkiinnostuneesti monenlaisista asioista.

Jos käyttää esimerkiksi 10% elämästään typeriltä vaikuttaviin asioihin, ei menetys ole kovin suuri vaikka ne aivan oikeasti olisivatkin typeriä juttuja. Moni autistinen henkilö kärsii pahasta jumiutuneisuudesta koska ei raaski tehdä tällaista uhrausta. Uskon että myös autismiin liittyvä sosiaalinen kömpelyys johtuu pitkälti samasta asiasta. Jos heittäytyy mukaan monenlaisiin tilanteisiin monenlaisten ihmisten kanssa, on suorastaan väistämätöntä että sosiaaliset taidot ja ymmärrys karttuvat. Itsessäni on tapahtunu todella suuri muutos ikätasoon nähden kömpelöstä jokseenkin sujuvaan.



























<!--“Mä olen tällanen autisti joka käyttää aikaa näin typeriin juttuihin.”

Päänuppi yksin ei vain riitä löytämään ajattelusta vikoja.

“Puhuu paljon, ei huomioi muiden puhetta” “Toisen asemaan vaikea eläytyä; sosiaalisten kontaktien ymmärtäminen puutteellista,” minusta kirjoitettiin kun olin lapsi.-->































