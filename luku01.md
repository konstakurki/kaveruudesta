# Sosiaalinen talous

> [Jos haluu saada, on pakko antaa.](https://www.youtube.com/watch?v=1RpsYx0i49E)
>
> —MC Nikke T

Jos Gamma haluaa kaverustua Deltan kanssa, on Gamman tehtävä se Deltalle kannattavaksi. Muuten Deltaa vain ärsyttää eikä hän viitsi tehdä yhteistyötä Gamman kanssa. Mitään ei saa ilmaiseksi—kaikessa on kyse taloudesta.

Tämä ehkäpä raadolliselta kuulostava seikka on puhtaan biologinen fakta: jokainen tuhlattu hetki ja kalori on pois lisääntymismahdollisuuksista. Miljardien vuosien evoluutio on varustanut meidät aivoilla jotka pakottavat meidät optimoimaan resurssiemme käyttöä. Kyse on alitajuntaisesta toiminnasta, mutta sen voi havaita jos asiaan kiinnittää huomiota.

## Taloustieteen perusteet

Sana “talous” kuulostaa kylmältä. Tämä johtuu siitä että olemme tottuneet puhumaan rahataloudesta jossa huomio on kiinnittynyt yhteen €-arvoiseen numeroon. Rahataloudessa päätöksenteko sujuu parhaiten heittämällä tunteet nurkkaan ja käyttämällä matemaattisia kaavoja. Tämä tuntuu robottimaiselta, ja itseasiassa suuri osa nykyajan osakekaupasta onkin robottien käymää. Ne pystyvät ajamaan yksinkertaisia optimointialgoritmeja huomattavasti nopeammin ja tarkemmin kuin ihminen.

Rahatalous ei ole koskaan ollut muuta kuin muovinen jäljitelmä ihmissuhteiden sosiaalisessa taloudessa. Sosiaalisessa taloudessa ei matematiikalla pitkälle pötkitä. Tunteet ovat se optimointikoneisto jonka evoluutio on meille antanut, ja ilman niitä ei sosiaalisessa taloudessa toimimisesta tule mitään. Tunteet ja numerot sopivat huonosti yhteen minkä seurauksena prostituutio, terapia ja kerjääminen ovat ongelmallisia konsepteja. Myös arvostelu koluissa toimii huonosti samasta syystä.

Gamma voi hyvin suurella todennäköisyydellä tehdä itsestään kannattavan ihmisen Deltalle antamalla mahdollisimman paljon itsestään siten että Deltalle on siitä mahdollisimman vähän kuluja. Tässä mielessä sosiaalinen talous toimii täsmälleen kuin rahatalous. Kun ensimmäistä kertaa ymmärsin rahatalouden ja sosiaalisen talouden yhtäläisyydet kristallinkirkkaasti, olin niin innoissani että tein eräälle ihmiselle aivan eksplisiittisen tarjouksen:

> Minä olen halukas jakamaan kanssasi aikaa vaikkapa tunnin verran. Jos ilmoitat paikan, tulen sinne, ja mikäli kykenen, siihen aikaan mikä sinulle sopii parhaiten. Jos haluat mieluummin jutella puhelimessa, niin sekin sopii milloin vain. Tällä ei ole kiire—jos esimerkiksi viiden vuoden päästä yhtäkkiä koet että transaktio olisi kannattava, anna palaa. Pidä toki mielessä että teknisistä syistä johtuen tarjous umpeutuu toisen meistä kuollessa.

Mitä Gammalla on tarjota Deltalle?

*   Gammalla on kokemuksia joita Deltalla ei ole.
*   Gamman persoonallisuus, tapa lähestyä ongelmia ja huumorintaju ovat Deltalle uutta.
*   Gamma tietää ainakin jotain mitä Delta ei tiedä.

Jos Delta on normaali kaveruuteen kykenevä sosiaalinen ihminen, nämä asiat kiinnostavat häntä. Kuinka voimakkaasti ja kuinka pitkän aikaa riippuu tietysti tilanteesta, mutta kaikissa ihmisissä on jotain mielenkiintoista.

Gamman haaste on rohkeudessa. Hänen on uskallettava olla avoin ja innokas; muuten Delta ei saa Gammasta irti sitä mitä hänessä on. Jos Delta tarjoaa Gammalle ongelmaa ratkaistavaksi, on Gamman uskallettava pureskella sitä ja kerrottava näkemyksensä.

Rohkeus on Gammalle vaikeaa, koska Gamma ei halua olla taakka Deltalle. Gamman empatia ei tässä tilanteessa toimi rakentavasti—jos Gamma on Deltan seurassa, on Deltan huomio joka tapauksessa kohdistunut Gammaan, joten himmailu ei auta. Järkeviä vaihtoehtoja on vain kaksi: olla Deltan seurassa täysillä tai kokonaan jossain muualla.

Monesti ympäristön asettama konteksti aiheuttaa paineita. Jos paikalla on paljon ihmisiä, voi olla vaikeaa saada aikaa toiselta. Jos tilanne on muodollinen, voi sääntöjen noudattamisen paine tehdä avoimuudesta mahdotonta. Tapaamalla kahden kesken rennossa ympäristössä on helpompaa olla rohkea.

Rohkeuden puutteen ja varautuneisuuden voi myös kääntää voitoksi. Joskus ollessani epävarmuuden vallassa sanon, “Sori kun olen huonoa seuraa, mä olen tällanen ujo ja autistinen paska ja mulle on hankalaa olla uudenlaisissa sosiaalisissa tilanteissa.” Jää rikkoutuu ja hyvällä tuurilla varautuneisuus ja jännitteet poistuvat kokonaan. Lisäksi pöydällä on uusi mielenkiintoinen puheenaihe.

Heikko itsetunto on yksinkertaisuudessaan kroonista, kokonaisvaltaista rohkeuden puutetta sosiaalisissa tilanteissa. Siihen voi olla monia syitä, mutta varmasti yksi merkittävimmistä on yhteiskunnan—koulun, työpaikan, Facebookin, TV-sarjojen—tarjoamat mallit. On helppo huomata, että ne ovat valheita: kysyt vain itseltäsi, “Mitä annettavaa tuolla roolimallilla voisi olla minulle?” Vastaus on mitä luultavimmin, “Ei mitään.” Nyt tiedät, että kyseistä roolimallia ei kannata mukailla, ainakaan mikäli haluat kavereita.

Mitä Gamma maksaa Deltalle?

*   Aikaa.
*   Energiaa.

Gamman kannattaa yrittää tavata Deltaa silloin kun ja siinä paikassa missä se Deltalle parhaiten sopii. Eräs kaverini on todella kiireinen koulutöidensä parissa, eikä hänellä ole juuri koskaan aikaa nähdä minua. Ratkaisin ongelman hoitamalla itseni hänen ovensa eteen aamulla ja kävelemällä hänen kanssaan kouluun. Sillätavalla hänen ei tarvinnut tehdä minkäänlaisia aikataulumuutoksia minun takiani.

Gamma kuluttaa Deltan energiaa sitä enemmän mitä kovemmin hän odottaa Deltan ratkaisevan hänen ongelmiaan. Delta voi tietysti innostua ratkomaan niitä, mutta hänelle pitää jättää mahdollisuus olla auttamatta ilman syyllisyyden tunnetta. Jos Gamma kykenee kertomaan ongelmistaan velvoittamatta Deltaa, voivat ne olla todella mielenkiintoisia ja toimia syvän keskustelun alkupisteinä.

Gamma kuluttaa Deltan energiaa myös, mikäli hän odottaa Deltan pitävän hänestä. Jos Delta tykästyy Gammaan, on se tietysti hienoa. Jos näin tapahtuu, on se boonusta. Jos Gamma ei odota Deltalta mitään, maksaa Gamma hänelle ainoastaan aikaa, ja hänen on helppo olla Gamman seurassa. Silloin Gammalla on itseasiassa parhaat mahdollisuudet saada Deltalta jotain.

Kuinka voi tehdä työtä jonkun ihmisen eteen haluamatta tältä yhtään mitään? Vastaus on, käsittämällä kyseisen henkilön palaksi potentiaalisten kavereiden joukkoa. On mahdotonta tehdä työtä odottamatta siitä mitään palkintoa—tosielämässä ei pyyteettömyyttä ei ole olemassa—mutta jos työskentelee yleisellä tasolla kaveruuden eteen, palkinnoksi käy kuka tahansa kaveri. Yksittäisillä ihmisillä ei silloin ole niin väliä.

Jos Gamma kykenee tähän ollessaan erittäin kiinnostunut Deltasta, kiinnostuu Delta lähes varmasti Gammasta. Kiinnostunut mutta vaatimaton Gamma on potentiaalisesti erittäin hyvä yhteistyökumppani Deltalle.

Tekemällä järkeviä ratkaisuja on mahdollista tehdä itsestään ainakin lyhyen tapaamisen arvoinen melkein kenelle tahansa. Esimerkiksi kolme varttia on alle 0.01% vuodesta mutta riittää kuitenkin kunnon keskusteluun. Lyhykäisyydessään strategia on tämä:

1.  Hoida tapaaminen mahdollisimman helpoksi hänelle. Jos mahdollista, voit tietysti mennä tapaamaan häntä ilman sopimista. Myös puhelinsoitto käy.
2.  Mene paikalle alasti (vertauskuvallisesti).
3.  Kun henkilö ei ole enää kiinnostunut sinusta, sano moi ja vaihda maisemaa.
4.  Käy tapahtumat läpi päässäsi ja mieti mitä voit tehdä paremmin seuraavalla kerralla (saman tai eri henkilön kanssa).

Kuulostaa siltä, että että Gamman on tehtävä paljon enemmän työtä kuin Deltan. Näin saattaa hyvinkin olla! Jos Gamma arvostaa Gamman ja Deltan välistä aikaa enemmän, hän myös tekee enemmän sen eteen. Rahatalouden kontekstissa tätä ei kyseenalaista kukaan.

Se että ihmissuhteen tulisi olla tasapuolinen joka suhteessa on myytti. Se kumpuaa siitä, että usein epätasapuolisessa ihmissuhteessa toinen osapuoli huijaa toista. Tästä sitten virheellisesti yleistetään että kaikissa epätasapuolisissa ihmissuhteissa vähemmän työtä tekevä huijaa, ja että kaikkia paitsi täysin tasapuolisia ihmissuhteita tulee karttaa. Tämä on umpityperä ajatus: täysin tasapainoisen ihmissuhteen tilastollinen mahdollisuus on täsmälleen pyöreä nolla. (Yksittäisen pisteen Lebesgue-mitta on nolla.)

Deltalla voi olla vaikka mitä syitä olla arvostamatta Gamman ja hänen välistä aikaa yhtä paljon kuin Gamma. Ehkä hänellä on paljon enemmän ystäviä kuin Gammalla. Ehkä hän on todella kiireisessä elämäntilanteessa. Ehkä hän ei ole hoksannut kuinka hieno juttu kaveruus on.

Minua ei haittaa pätkääkään jos joudun tekemään enemmän työtä. Hyvin usein teen sitä reippaasti enemmän kuin toinen osapuoli. Siten minulla on paljon kavereita—ja heillä on minut. Kaikki voittavat! `:)`

## Esimerkkianalyysi

Olen lukenut erään [Pieter Hintjensіn](http://hintjens.com/) kirjoituksia enemmän kuin kenenkään muun. Joitain hänen tekstejä olen lukenut useaan kertaan. En ollut kerennyt lukea Pieteriä paljoakaan kun olin jo päättänyt että jonain päivänä tapaan hänet.

Viime huhtikuun 19. päivänä kuulin, että Pieterillä oli syöpä ja mahdollisesti vain viikkoja elinaikaa. Minun oli pakko kysyä häneltä heti sähköpostitse että sopisiko tulla käymään. Hän vastasi välittömästi, “Tule Brysseliin niin nopeasti kuin pystyt.” Kului kolme päivää ja olin sairaalassa juttelemassa hänelle.

Jutustelimme kahdestaan kolmisen tuntia. Pieter pyysi minua tyhjentämään hänen urinal?. Havaitsin [Ben Franklin -ilmiön](https://en.wikipedia.org/wiki/Ben_Franklin_effect): jos Gamma tekee pienen palveluksen Deltalle, alkaa Gamma pitää Deltasta enemmän (eikä toisinpäin). Pieter vitsaili, “Konsta tuli Suomesta katsomaan kun hänen lempikirjailiansa kusee pulloon!”

Kärsin siitä että arvostan Pieteriä niin paljon: usein en uskaltanut sanoa mitä ajattelin. Kun kysyin häneltä erään psykopaatteihin liittyvän kysymyksen, hän vastasi samalla tavalla kun kirjassaan jonka olin lukenut kolme kertaa, mutta en sanonut että olisin halunnut kuulla jotain uutta. Toisaalta en sortunut jumaloimaan häntä; en pyytäny nimikirjoitusta tai hölmöillyt muuten.

Tapasin myös Pieterin äidin ja siskon. Oli mielenkiintoista keskustella heidän kanssaan ja nähdä kuinka Pieterin läheiset suhtautuivat häneen. Seuraavana päivänä menimme vielä kolmestaan tapaamaan Pieteriä. Silloin minulla oli pieniä vaikeuksia, sillä en osannut oikein nähdä että kuinka paljon Pieter halusi olla pelkästään perheensä kanssa ja kuinka paljon myös minun kanssani. Väistyin käytävään ehkä turhan usein; toisaalta silloin kun olin Pieterin huoneessa, en uskaltanut sanoa paljoakaan. Nukuin äidin ja siskon luona kaksi yötä ja viimeisenä päivänä mokasin: en älynnyt että minun olisi jo syytä jatkaa matkaani. Äiti joutui lähettämään minut matkoihini.

Kuukauden päästä Pieter oli valmistelemassa läksiäisjuhlia. Matkustin uudestaan Brysseliin. Kyllä, arvostan Pieteriä todella paljon ja laskeskelin että minun kannattaa lähes hinnalla millä hyvänsä koettaa päästä juttelemaan hänen kanssaan ja kaivamaan hänestä viisautta ulos. Varasin varmuuden vuoksi neljä yötä hostellista.

Menin etkoille ja kohtasin suuria ongelmia. Tällä kertaa en ollut Pieterin kanssa kahdestaan, vaan paikalla oli Pieterin kavereita. Minun oli mahdoton ymmärtää itselleni vierasta kulttuuria, enkä osannut kommunikoida ollenkaan. En uskaltanut ottaa riskejä ja niinpä vain burgeroin hiljaa itsekseni, irrallani muista. Pieter jopa sanoi minulle, “Taidat olla hyvin autistinen kaveri.”

Pelokkuudestani johtuen en ollut kovinkaan hyvä vieras etkoilla. Minusta alkoi tuntua kuokkavieraalta, ja lopulta pelkäsin pilaavani tunnelman niin voimakkaasti että päätin jättää varsinaiset juhlat kokonaan väliin. Hengalin sitten Brysselissä neljä päivää ilman sen kummempaa tekemistä.

Jälkeenpäin ajateltuna toimin todella typerästi. Pieter oli esittänyt yleisen kutsun juhliin Belgian televisiossa, joten on täysin järjetön ajatus että olisin ollut kuokkavieras. Olisin voinut olla paikalla vaikka hiljaa nurkassa istuen ja meininkiä maistellen. Se ei olisi voinut pilata kenenkään tunnelmaa. Alkoholin avulla olisin varmaankin vieläpä päässyt pelostani eroon ja kyennyt olemaan positiivinen lisä porukkaan.

Lokakuun neljäntenä päivänä Pieter twiittasi:

> I’m choosing euthanasia etd 1pm.  
> I have no last words.

Tämä on niin Pieteriä: ei paskapuhetta, ei edes kuoleman porteilla. Käsittääkseni häntä ei ole kuopattu; hän kirjoitti jättävänsä hautajaiset väliin ja antavansa ruumiinsa lääketieteelle. Mutta kuollut hän on. Nyt ei kenelläkään ole enää mahdollisuutta kaveerata hänen kanssaan. Virheitäni—jätin juhlat väliin, ja jätin myös soittamatta Pieterille Skypellä vaikka siitä oli ollut puhetta—en voi paikata. Mutta voin ottaa niistä opiksi ja iloita siitä että ylipäätäni sain tuntea hänet.

Pieterin suhtautuminen kuolemaan oli raa’an pragmaattinen. Hän kirjoitti siitä artikkeleita. Niistä erityisesti [*Five Years, Five Wishes*](http://hintjens.com/blog:109) ja [*A Protocol for Dying*](http://hintjens.com/blog:115) ovat lukemisen arvoisia.

## Luottamus

Se miten kuvailin toisen lähestymistä saattaa kuulostaa hämärältä. Sitä se onkin: suurin osa kaverisuhteista syntyy sattumalta, ja se että joku määrätietoistesti yrittää tehdä tuttavuutta on todella outoa. Niinkuin rahataloudessa, myös sosiaalisessa taloudessa liikkuu huijareita, ja tilanne voi näyttää siltä että Gamma pyrkii käyttämään Deltaa hyväksi.

Rahatalouden uusimpia tuulia on Bitcoin. Bitcoinia louhitaan kirjaamalla transaktioita niinsanottuihin lohkoihin. Palkinnoksi lohkon louhija saa tietyn määrän upouutta Bitcoinia—tästä nimitys “louhiminen”. Huijausten välttämiseksi on tärkeää että lohkon louhiminen on työlästä. Niimpä lohkon louhijan tuleekin toimittaa niinsanottu *proof-of-work* (POW): hänen pitää lisätä lohkoon bittejä siten että sen SHA-256-tiiviste alkaa tietyllä määrällä nollia. Ainut tunnettu menetelmä tähän on summamutikassa kokeileminen, ja se on työlästä. Kun louhija toimittaa POW:n, on varmaa että hän on tehnyt työtä sen eteen.

Sama pätee ihmissuhteisiin. Gamman on toimitettava Deltalle jatkuvasti POW:ja osoittaakseen että hän ei huijaa. Gamman on näytettävä ymmärtävänsä Deltaa ja osallistuttava hänen pöydälle nostamiensa ongelmien ratkaisuun. Mutkien oikominen ei auta—jos Gamma esimerkiksi osoitta innokkaasti tietävänsa Deltasta pinnallisia yksityiskohtia, saa se Deltalle aikaan vain epämääräisen fiiliksen joka haittaa yhteistyötä.

Jotta Gamma voisi ymmärtää Deltaa ja ratkaista tämän ongelmia, on Gamman tiedettävä Deltasta asioita. Mikäli Delta ei spontaanisti vuodata elämäntarinaansa Gammalle, on Gamman esitettävä kysymyksiä. Tämä saattaa muodostua ongelmaksi—toisen asioista uteleminen haiskahtaa epärehelliseltä puuhalta. Eräs ihminen kertoi minulle ohimennen kärsivänsä rajusta sosiaalisten tilanteiden pelosta. Halusin kysyä asiasta, mutta en uskaltanut sillä pelkäsin sen laukaisevan hänen psykopaattihälytyskellonsa soimaan. Jälkeenpäin ajateltuna olisi ehkä kuitenkin pitänyt kysyä. Oli miten oli, kyse on tietysti aina kompromisseista ja tasapainon hakemisesta.

Luottamuksen rakentamisessa POW:ien lisäksi olellista on antaa dataa omasta itsestään ja elämästään.

> Mä olin just sirkusesityksessä ja mut haettii yleisöstä lavalle läiskii yhtä äijää perseelle...Se haki kaks tyyppii kattoo strippausesitystää, sit teki mieli läiskää sitä perseelle mut en kehannu, mut sit se oikee näytti et piiskaa ni läpsäsin sit pari kertaa `:D`

Datan tulee olla jokseenkin raakaa, sellaista mistä Gamma voi itse tehdä omia johtopäätöksiään. Pieniä asioita, paljon. Idea ei ole että Delta kiinnostuu niistä sen kummemmin; kyse on tarttumapinnasta. Gamman tulee näyttää millainen ihminen hän oikein on ja millaista elämää hän elää.

Luottamukseen vaikuttaa lisäksi suuri määrä muita pieniä asioita. Esimerkiksi aurinkolasit luovat etäisyyttä ja antavat huijarille mahdollisuuden peittää katseensa. Itse en käyta aurinkolaseja koskaan.

Tutustuminen on vuoropuhelua, ja todellinen luottamus syntyy repliikki repliikiltä. Gamma kertoo Deltalle itsestään jotain pientä, ja Delta vastaa pienellä tiedonpalasella omasta elämästään. Gamma osoittaa ymmärtävänsä Deltaa pikkuisen. Gamma kertoo jotain, Delta vastaa ja kertoo lisää ja niin edelleen. Nykyään vuoropuhelu on usein digitaalisten viestien vaihtamista. Hyvä puoli on se että tutustuvien henkilöiden ei tarvitse olla samassa paikassa eikä heidän tarvitse olla samaan aikaan aktiivisena. Huono puoli on se, että informaatiovirta on rajoittunut.

Huijarilta vaikuttaminen ei ole Gamman ainoa vaaranpaikka. Mikäli Deltasta tuntuu siltä, että Gamma on rakastunut tai muulla tavoin leimautunut häneen, saattaa hän pelätä että Deltalle ei kohta riitäkään kohtuullinen määrä yhteistä aikaa, tai että Gamma pyrkii perustamaan Deltan kanssa jotain liian suurta, esimerkiksi perheen, orkesterin tai yrityksen. Delta saattaa torjua Gamman tai suhtautua häneen varauksellisesti.

Välttääkseen tällaisia ongelmia on Gamman osoitettava kiinnostusta ainoastaan yhteistä aikaa kohtaan. Gamma voi osoittaa että hän ei ole mustasukkainen keskustelemalla rennosti ja avoimesti kriittisistä aiheista kuten Deltan puolisosta tai bändistä. Gamma voi myös kertoa muista Deltan kaltaisista kavereistaan, mutta tässä on se ongelma että se kuulostaa Deltan vähättelyltä.

Gamman pitää tietysti antaa Deltalle reippaasti aikaa ilman Gammaa. Tämä on kriittinen asia erityisesti mikäli Gamma on jostain syystä herättänyt epäluottamusta Deltassa. Antamalla Deltalle aikaa Gamma osoittaa että hän kunnioittaa Deltaa ja kykenee reguloimaan tunteita jotka häntä Gammaa kohti vie ja toisaalta että hän on kiinnostunut Gammasta muutenkin kuin pelkällä pinnallisella tasolla. Minulle tämä on usein erittäin hankalaa—innostuessani jostain tyypistä en meinaa muistaa kuinka häiritsevältä se tuntuu jos toinen pommittaa jatkuvasti viesteillä ja haluaa nähdä.

<!-- Puhelin on loistava työkalu. Jos se tuntuu sinusta kiusalliselta tai ahdistavalta, opettele siitä eroon. //// Antin tulee välttää lupauksia ja vaatimuksia. -->

## Keskusteleminen

Keskustelussa nämä taloustieteen perusperiaatteet toimivat reaaliajassa. Jokainen repliikki on transaktio jolla keskustelija hakee mahdollisimman taloudellista lopputulosta. Sanat maksavat aikaa ja ajatuksia sekä puhujalta että kuuntelijalta, joten niitä on käytettävä säästeliäästi.

Minulla on sellainen mielikuva, että useimmiten keskustelutaidoilla tarkoitetaan sitä, että osaa sanoa toiselle sellaisia asioita mitkä saavat hänet hyvälle mielelle. Lapselle opetetaan, että tietynlaisten asioiden, esimerkiksi lyömisen, jälkeen sanotaan anteeksi. Ajatellaan, että keskustelutaito on kyky manipuloida toisen tunnetilaa ja sitämyötä käytöstä. Irvokkaimmat esimerkit tästä ilmiöstä löytyy varmaankin iskurepliikeistä.

Sosiaalisen huijarin näkökulmasta katsottuna manipulointi on keskustelutaoitoa. Minulle keskustelutaito on yhteistyötä käyttäen sanoja, ilmeitä ja muita viestejä taloudellisella tavalla. Millaiset sanat toimivat yhteistyössä? Tähän on helppo vastata: kysyn itseltäni, “Millaisia sanoja haluan kuulla kavereiltani?” Vastaus on, “Rehellisiä sanoja.” Mikä on taloudellista sanailua on yhtä helppo kysymys. “Mitä asioita haluan kaverieni sanovan minulle?” “Sellaisia, jotka auttavat minua käytännön ongelmissa.” Lyhykäisyydessään keskustelutaito on taitoa ilmaista ilmaista relevantteja tosiasioita. Kaikki keskusteleminen—olipa kyse vitsailusta, riitelystä, flirttailusta tai matemaattisesta todistamisesta—hoituu samalla periaatteella.

Minä en kehu kovin usein. Se johtuu siitä, että minulle tulee harvoin sellainen olo että kehun ilmaiseminen olisi taloudellista. Ihmiset tietävät että silloin kun sanon jotain positiivista, siihen voi luottaa. Mitä vanhemmaksi tulen, sitä enemmän kehun ihmisiä. Se johtuu siitä, että alan pitää yhä laajemmasta skaalasta juttuja, ja alan nähdä useampia syitä miksi toisen olisi hyvä tietää että arvostan jotain hänessä.

Joskus tietysti päädyn keskustelemaan manipuloivalla tavalla. Seuraukset huomaa heti: toinen henkilö ei keskustele minun vaan esittämäni roolihahmon kanssa, ja kahden ihmisen välinen yhteys jää toteutumatta. Yläasteella olin huonon käytöksen takia siirretty pois äidinkielentunneilta. Halusin tunneille takaisin, joten valehtelin opettajalleni olevani pahoillani aiheuttamastani häiriöstä. Muistan kuinka typerältä se tuntui. Nykyään ilmaisen pahoillani olemista vain jos olen pahoillani. Saatan silloin käyttää sanaa “anteeksi”, koska sen merkitys Suomen kielessä vastaa tarkoitusperää kyseisessä tilanteessa. Saatan käyttää myös muita ilmaisuja jotka ajavat saman asian.

Keskustelun aloittaminen vieraan ihmisen kanssa on hankalaa, koska kumpikaan ei tiedä toisesta mitään. Silloin on pakko tyytyä epämääräisiin arvioihin siitä, mikä saattaisi olla relevanttia. Äsken sanoin uudelle tuttavuudelle, “En ostanut viime Ilosaarirockiin lippua, koska pääesiintyjänä oli Cheek.” Tämä on relevanttia, mikäli keskustelukumppanini ei pidä Cheekistä: silloin voimme raivota hetken aikaa yhdessä ja purkaa stressiä. Mikäli hän pitää Cheekistä, kommenttini ei ole relevantti, sillä hän on kuullut Cheekin huonoudesta ennenkin. Kommentti on kuitenkin turvallinen, sillä harva Cheek-fani raivostuu negatiivisesta kommentista. Pientä kiusallisuutta pahempaa tuskin on luvassa.

Yleisemmin ottaen joskus hiljaisuus on keskustelussa paikallaan, mutta usein se on kiusallista ajanhukkaa. Silloin on sovellettava samaa ideaa kuin keskustelun aloittamisessa: sanottava jotain satunnaista. Yleisiä pelkoja ovat, “Mitä jos se kuulostaa teennäiseltä?” ja “Mitä jos sanon jotain, mikä loukkaa?” Vastaukset ovat “Aivan sama” ja “Sitten pöydällä on konflikti joka inspiroi syvempään keskusteluun.” Tietysti joskus konflikti voi olla tuhoisa, mutta sellaisessa tapauksessa on todennäköistä ettei kaveruudesta muutenkaan tulisi yhtään mitään.

Vaikka tuollainen Cheek-kommentti saattaa toimia jäänsärkijänä, negatiiviset kommentit ovat usein epärelevantteja. Jos ajattelen että kaverini tatuointi on ruma, jätän sen sanomatta, koska se on ja pysyy iholla, ja kaveri tuskin kunnioittaa minua tatuointimielipidettäni. Toisaalta, jos kaverillani on sepalus auki, sanon siitä, koska asia on helppo korjata ja kaverini on luultavasti halukas sen tekemään. Negatiiviset kommentit muista, esimerkiksi “Katso, kuinka typerän näköinen tyyppi tuolla kävelee!” Usein se on virhe, tosin usein se on myös toimiva instanssi sadistista huumoria jonka jaan joidenkin kaverieni kanssa.

Vakavampia ikäviä kommentteja on syytä miettiä erittäin pitkään. Kuulin kerran että eräs kaverini oli pettänyt poikaystäväänsä. Tilanne oli ikävä ja valheet syviä. Mietin kaksi viikkoa, pitäisikö minun puuttua asiaan. Lopulta sanoin kaverilleni, “Tiedän mitä olet tehnyt, ja minusta sinun olisi syytä kertoa siitä poikaystävällesi.” Kommentin ja siitä seuraavien asioiden myötä menetin kaverin, ja poikaystävä uhkasi minua sekä väkivallalla että lähestymiskiellolla. Oliko sanomiseni relevanttia? En tiedä. Mielestäni valheet olivat liian rankkoja; toisaalta pettäminen ei kyseisessä tilanteessa kaikkien muiden ongelmien ohella ehkä edes merkinnyt kovin paljoa. Toisaalta ainoa vaihtoehto jatkaa kaveruutta olisi ollut valehteluun osallistuminen.

Keskustelussa on syytä olla rehellinen, mutta tämä ei tarkoita sitä että ainoastaan absoluuttisia totuuksia tulisi päästää suustaan. Jos sanon tehneeni viime keskiviikkona jotain, ja yhtäkkiä muistan että se olikin tiistaina, ei virhettä ole syytä korjata, sillä se todennäköisesti maksaisi turhan paljon aikaa. Keskustelukumppani ei myöskään luultavasti muista päivämäärän kaltaisia yksityiskohtia kovin pitkään.

Kirjaimellinen totuus on epärelevantti myös sellaisissa tilanteissa, missä nonverbaalinen viestintä kertaa asian todellisen laidan. “Ihan OK,” sanon kun kaveri on tehnyt uudenlaista ruokaa. Molemmat tiedämme että se tarkoittaa, “Paskaa.” Kirjaimellinen suorapuheisuus olisi virhe, koska tässä kontekstissa se tarkoittaisi tarkoituksellista loukkausta. Tietysti on hyvä jos kulttuuri on sellainen että kirjaimellinen suora puhe on aina OK. Ainakin yhden kaverin kanssa meidän välillä vallitsee tällainen kulttuuri.

Se, millaista keskustelu käytännössä on, riippuu suhteessa esiintyvästä luottamuksesta. Joidenkin ihmisten kanssa ammun suustani asioita ilman sen kummempaa suodatusta. “Saatanan idiootti, puhut täyttä paskaa.” Sellaiset ihmiset luottavat minuun ja tietävät että sanon yleensä jotain järkevää, tai ainakin yritän tehdä yhteistyötä. Toisten kanssa olen maltillinen, koska he eivät luota sanoihini niin paljoa. Luottamus tietysti riippuu puheenaiheesta: toiset luottavat minuun toisissa asioissa ja toiset toisissa.

<!--Olen puhunut tässä relevanssista. Se mikä on relevanttia on tietenkin kaikkea muuta kuin itsestäänselvä asia.-->

## Olemmeko itsekkäitä?

Markkinataloudesta puhuttaessa ajatellaan usein että ihmiset ovat itsekkäitä. Jokainen pyrkii maksimoimaan omat voittonsa, ja jos hyvin käy, itsekkäistä yksilöistä muodostuu Adam Smithin näkymätön käsi joka hoitaa asiat siten että kaikilla menee hyvin.

Sosiaalisessa taloudessa asia ei ole näin yksinkertainen. Yrittääkö jokainen maksimoida omaa hyötyään? Tähän on vaikea vastata, sillä “oma hyöty” ei ole kovin tarkka käsite. Jos itsekkäällä tarkoitetaan sitä, että päättää itse mihin omat resurssinsa käyttää, ovat kaikki loppukädessä triviaalisti itsekkäitä otuksia. “Itsekäs” on aika hyödytön sana jos se kuvaa universaalisti kaikkia ihmisiä.

Se, mitä ihminen sosiaalisessa taloudessa tavoittelevat, riippuu olosuhteista ja hänen persoonastansa. Jos ruokaa on vain yhden leipäpalan verran, jokainen yrittää silloin saada tuon palasen itselleen. Tuntuu käytännön järkevältä sanoa, että tuolloin ihmiset käyttäytyvät itsekkäästi—olosuhteiden pakottamana. Jos ruokaa on kaikille ja ihmiset elävät vahvassa ja terveessä yhteisössä, on ruoan antaminen kaverille itsestäänselvyys. Tällöin käytännön järjen mukaan ihmiset käyttäytyvät epäitsekkäästi.

Erittäin hyvissä olosuhteissa sosiaaliset ihmiset auttavat toinen toistaan. He haluavat kaikki samaa asiaa. Jokainen päättää itse mihin resurssinsa käyttää, mutta nämä halut ovat sopusoinnussa keskenään. Vaikuttaa siltä, että kaikki ihmiset—noin 1%-10% populaatiosta—eivät kuitenkaan ole sosiaalisia. Jotkut elävät elämäänsä huijaten eivätkä tee yhteistyötä vaikka olosuhteet olisivat kuinka hyvät. He ovat sosiaalisen talouden huijareita, ja heitä voidaan kutsua esimerkiksi antisosiaalisiksi ihmisiksi tai psykopaateiksi.

Mielestäni on järkevää sanoa, että ihminen on itsekäs, mikäli hän ei hyvissäkään olosuhteissa tee yhteistyötä. Epäitsekäs hän taas on mikäli yhteistyöhalukkuutta löytyy. Lienee sanomattakin selvää että tätä kirjaa kirjoitan epäitsekkäitä ihmisiä varten, mikäli hyväksymme esittämäni määritelmän itsekyydelle. <!--Markkinataloudessa sosiaalinen itsekkyys on tunnetusti etu: [toimitusjohtajan ammatissa työskentelee eniten psykopaatteja](http://time.com/32647/which-professions-have-the-most-psychopaths-the-fewest/).-->

## Resurssit

Ihmissuhteiden tietoinen rakentaminen on työlästä. Se vaatii paljon aikaa ja voimia, ja voi kuulostaa siltä ettei sitä jaksa tehdä ollenkaan.

Lähtökohtaisesti meillä on aikaa: vuodessa on 8766 tuntia. Voimia meillä on myös, siihen asti kunnes tuhlaamme ne. On koulu, palkkatyö ja harrastukset. Nämä kaikki syövät resursseja jotka voisimme käyttää kaveruuteen. Monen kohdalla näyttää siltä että kaveruudelle ei jää resursseja ollenkaan. Kavereita halajavan tulee siis vapauttaa itsensä oravanpyörästä, tai ainakin säätää sitä pienemmälle kierrosluvulle ja vähentää siellä viettämäänsä aikaa.

Aloitetaan harrastuksista. Hyvä harrastus on sellainen, joka tuottaa koko ajan iloa ja jota haluaa tehdä. Jos huomaat usein ajattelevasi, “Ei perkule, taas reenit, pitäsköhän jättää väliin?” on syytä miettiä harrastuksen järkevyyttä. Jos harrastus vie paljon rahaa ja pakottaa tekemään rankasti palkkatyötä, on siinä kanssa syy miettiä asiaa.

Itse harrastin yhden kesän laskuvarjohyppyä. Hyppäsin noin kaksikymmentä kertaa. Hyppääminen on hienoa, mutta minulla oli jollain tavalla huono fiilis hommasta. Pohdin asiaa ja tulin siihen tulokseen että hyppytoiminnan tarkasti organisoitu luonne oli liian stressaavaa minulle. Eräänä hyppypäivänä, kun olin jo pukenut varjon selkääni, minulle kirkastui että minun ei kannata jatkaa. Riisuin varjon selästäni ja pyöräilin pois kerholta. En ole hypännyt sen koommin.

Moni harrastaa tullakseen taitavaksi. Taitavuutta taas haetaan näennäisesti ilman syytä, aivan kuin se olisi jonkin sortin itseisarvo. Se ei ole totta—olisitko iloinen taidostasi mikäli olisit varma että kukaan ei koskaan saa tietää siitä? Haluamme olla taitavia, koska haluamme olla tärkeitä ja hyväksyttyjä. Toisin sanoen, haluamme olla taitavia saadaksemme kavereita.

Todellisuudessa taidoilla ei saa kavereita. Itse aloin soittamaan kitaraa joskus kaksitoistavuotiaana. Nyt olen taitava soittamaan, ja saan halutessani sillä ihailua osakseni. Se tuntuu tietysti hyvältä, mutta kukaan ei ala työskentelemään kanssani kitaransoittotaidon takia. Nykyään en harjoittele kitaransoittoa enkä esittele taitoani muille. Tiedän että se ei olisi minulle kannattavaa.

Kun ymmärtää että usein motivaatio harrastamisen takana on kaveruus, voi asiaa pähkäillä uudesta perspektiivistä. Mitä jos valitsisin harrastukseni puhtaasti sen perusteella millaisia ihmissuhteita se tuo tullessaan! Laskuvarjohyppy olisi voinut olla hyvä tässä mielessä—kerholla on hyvä henki ja voimakas kulttuuri—mutta en kyennyt avautumaan siellä lukuisista saunailloista ja tyhjenneistä kaljatölkeistä huolimatta. Luulen että se johtui siitä että kerholla oli hankala tutustua ihmisiin kahden kesken; sitä oltiin aina porukassa.

Nykyään käyn lenkillä jos kaveri käy. Pelaan sulkapalloa tai Age of Empiresiä mikäli kaverit niin haluavat. Oikeastaan kaikki harrastukseni on tällä hetkellä ihmisten mukaan valikoituneita. Tämä pätee jopa soittoharrastukseeni: nuorempana etsin kavereita jotka olisivat soittaneet juuri sitä musiikkia mitä itse halusin. Ketään ei löytynyt ja bändi jäi perustamatta. Nyt soitan bändissä, koska kaverini pyysivät minua siihen. Soittaminen on kivaa ja virkistävää, mutta ennenkaikkea se tuo minut yhteen kolmen hyvän tyypin—ja joskus myös yleisön—kanssa.

Sitten koulutus. Oma kokemukseni on se että koulussa ei kovin paljoa asioita opita. Opiskelin fysiikkaa, ja opetussuunnitelmaa noudattamalla ei ollut mahdollista hankkia muuta kuin pinnallista laskutekniikkaa ja palaa loppuun. Hylkäsinkin opetussuunnitelman—ja valmistumishaaveet—ja opin paljon nopeammin ja syvällisemmin, kunnes tulin siihen tulokseen että maailmassa on fysiikkaa tärkeämpiä asioita. Mikäli uskot että voit pärjätä ilman koulutusta, olen sitä mieltä että niin kannattaa tehdä.

Toisaalta koulu antaa mahdollisuuden olla paljon ihmisten kanssa tekemisissä. Minunkin kavereista suurin osa on jonkun koulun kautta päätynyt tekemisiin kanssani. Jos koulua haluaa käydä, on viisasta valita vähätöinen ala jossa pääsee osaksi hyvää porukkaa.

Jäljellä on työpaikka. Sen tarve riippuu rahantarpeesta. Karsimalla rahantarvetta voi vähentää tarvetta työskennellä; esimerkiksi lainaa kannattaa ottaa niin vähän kuin mahdollista. Edellinen paikka missä asuin oli kolmenkymmenen hengen ekoyhteisö. Meillä oli 25 hehtaaria peltoa, 17 hehtaaria metsää ja 3300 neliömetriä lämpimiä tiloja. Itselläni oli viidentoista neliömetrin huone. Asumiskulut, sisältäen ruoan, olivat 300 euroa kuukaudessa. Sitten olin vuoden kodittomana ja rahattomana. Asumiskulut 0 euroa.

Nyt asun omakotitalossa kolmen ystävän kanssa. Meillä on piha jossa voi kesällä temmeltää alasti, pihasauna ja noin 3 kilometriä matkaa kaupungin keskustaan. Voiko parempaa edes toivoa? Minun osuus asumiskuluista ilman ruokaa on alle 300 euroa kuussa. Jos ruokaan, kaljaan ja nettiin menee vaikkapa 300 euroa kuussa ja bussilippu kaupungin vaihtamiseen maksaa kymmenen euroa, sellainen 600-700 euroa riittää kuukauden elämiseen. Tämä pätee kenelle tahansa velattomalle, lapsettomalle ja terveelle henkilölle eikä vaadi mitään asketismia.

Siispä, jos palkkasi on vaikkapa 2000 euroa, voit työskennellä vuoden ja pitää kaksi vapaata. Jos olet työtön ja saat työttömyysturvaa, tilanteesi on lähes täydellinen.

Seuraavaksi: [Luku 2. Käytännön kaveruus](https://gitlab.com/konstakurki/kaveruudesta/blob/master/luku02.md)
