# Muistiinpanot

## Rakenne

*   Esipuhe
*   Sosiaalinen talous
*   Käytännön kaveruus
*   Tunteiden hallinta
*   Huijarit
*   Seksuaalisuus
*   Liite: näkökulma yhteiskuntaan

*   (Käytännön menetelmiä)

## Joitakin pointteja

*   Pissakakkajutut
*   Terminologia: kaveri vs. ystävä
*   Samat periaatteet toimivat peruskaverisuhteiden
    sekä seksuaalisten suhteiden etsimisessä
