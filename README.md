# Kaveruudesta

Tämä on [Konsta Kurjen](http://konstakurki.co/) juuri aloittama kirja kaveruudesta ja ystävyydestä.

*   [Esipuhe: Epäonnistuminen](https://gitlab.com/konstakurki/kaveruudesta/blob/master/esipuhe.md)
*   [Luku 1. Sosiaalinen talous](https://gitlab.com/konstakurki/kaveruudesta/blob/master/luku01.md)

---

Copyright &copy; (2016) Konsta Kurki

Kirjaa saa levittää ja muokata [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) -lisenssin ehtoja noudattaen.

